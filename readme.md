# Taller de spring boot

## Objetivo del taller:
    Usar spring boot para crear un jar ejecutable (standalone), que atienda peticiones en una URL de nuestra pc, y retorne "hola mundo"

## Recomendaciones: 
    Ir siguiendo los pasos y buscando la info en la documentación oficial de spring boot.
    En caso de querer la resolución, pueden encontrarla en el archivo **taller-spring-boot.txt**, dentro de la carpeta doc. 

## Pasos
1. En netbeans crear un nuevo proyecto Maven -> Java Application
2. Agregar al pom las dependencias de Spring boot.
3. Crear una clase Controller, con un método que atienda las peticiones de "/" y retorne "hola mundo"
4. Ejecutar el jar en el netbeans y desde el navegador visitar: http://localhost:8080
5. Agregar lo necesario para que podamos ejecutar nuestra aplicacion usando solamente java (sin el netbeans). Pista: agregar algo al pom.
6. Hacer que la aplicación atienda en el puerto 9090. Pista: agregar un archivo de properties dentro de src/resources

## Extra
    Realizar las modificaciones necesarias para que nuestro módulo se convierta en un .war desplegable en un Tomcat.

## Extra2
    ¿y si quiero hacer un proyecto web, con spring, spring security, spring data y toda la bola? http://start.spring.io/